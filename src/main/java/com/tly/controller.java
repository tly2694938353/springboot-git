package com.tly;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("user")
public class controller {
    @Autowired
    @RequestMapping("toindex")
    public String toIndex(){
        return "redirect:queryUserList";
    }
}
